﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using Emgu.CV;

namespace Blank_Decoder
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private string imagePath;
        private Decoder decoder = new Decoder();

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            var image = new BitmapImage();
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                image = new BitmapImage(new Uri(openFileDialog.FileName));
                imagePath = openFileDialog.FileName;
            }

            RawImage.Source = image;

            int thresholdX = (int)ThresholdXSlider.Value, thresholdY = 4;

            // принудительная побуквенная идентификация текста
            // для идентификации текста побуквенно нам достаточно threshholdX выставить в 4
            if ((bool)SymbolCheckbox.IsChecked)
                thresholdX = thresholdY;
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                var contours = decoder.OpenImage(imagePath);
                var perSymbol = decoder.OpenImage(imagePath);

                contours = decoder.ResizeFrame(contours, 837, 587);
                contours = decoder.RotateFrame(contours, 30);

                perSymbol = decoder.ResizeFrame(perSymbol, 837, 587);
                perSymbol = decoder.RotateFrame(perSymbol, 30);

                // отображаем штриховку
                var erodeFrame = decoder.VisualErode(contours, thresholdX, thresholdY);
                CvInvoke.Imshow("ERODE", erodeFrame);

                // забираем 4 области с ответами и исправлениями ответов отделоьно
                contours = decoder.CropAndIdentify(contours, 675, 200, 295, 0, thresholdX, thresholdY);
                contours = decoder.CropAndIdentify(contours, 675, 200, 587, 290, thresholdX, thresholdY);
                contours = decoder.CropAndIdentify(contours, 770, 690, 587, 290, thresholdX, thresholdY);
                contours = decoder.CropAndIdentify(contours, 770, 690, 295, 0, thresholdX, thresholdY);

                // вывод контуров посимвольно
                perSymbol = decoder.CropAndIdentify(perSymbol, 675, 200, 295, 0, thresholdY, thresholdY);
                perSymbol = decoder.CropAndIdentify(perSymbol, 675, 200, 587, 290, thresholdY, thresholdY);
                perSymbol = decoder.CropAndIdentify(perSymbol, 770, 690, 587, 290, thresholdY, thresholdY);
                perSymbol = decoder.CropAndIdentify(perSymbol, 770, 690, 295, 0, thresholdY, thresholdY);
                CvInvoke.Imshow("PER_SYMBOL", perSymbol);

                IdentifiedImage.Source = decoder.Convert(contours);

                // итог
                // erodeFrame - хранит шриховку, для применения на ImageView достаточно прогнать его через метод decoder.Convert()
                // perSymbol - хранит посимвольное отображение, для применения на ImageView опять же прогоняем через decoder.Convert()
                // Нужные ROI срезаны в методе CropAndIdentify, их можно выдернуть по мере проходки по контурам
            }
        }
    }
}
