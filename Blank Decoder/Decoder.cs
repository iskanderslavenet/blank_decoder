﻿using System;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using System.IO;

namespace Blank_Decoder
{
    class Decoder
    {

        public BitmapImage Convert(Image<Bgr, Byte> src)
        {
            var ms = new MemoryStream();
            src.Bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            var image = new BitmapImage();
            image.BeginInit();
            ms.Seek(0, SeekOrigin.Begin);
            image.StreamSource = ms;
            image.EndInit();
            return image;
        }

        public Image<Bgr, Byte> OpenImage(string imagePath)
        {
            return CvInvoke.Imread(imagePath).ToImage<Bgr, Byte>();
        }

        // метод визуально отображает бинаризацию изображения
        public Image<Bgr, Byte> VisualErode(Image<Bgr, Byte> frame, int threshholdX, int threshholdY)
        {
            var kernel = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(threshholdX, threshholdY), new Point(-1, -1));

            CvInvoke.CvtColor(frame, frame, ColorConversion.Bgr2Gray);
            CvInvoke.Erode(frame, frame, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
            CvInvoke.Imshow("ERODE", frame);
            return frame;
        }

        public Image<Bgr, Byte> CropAndIdentify(Image<Bgr, Byte> frame, int heightCrMax, int heightCrMin, int widthCrMax,
            int widthCrMin, int threshholdX, int threshholdY)
        {
            // ядро для морфологических операций
            var kernel = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(threshholdX, threshholdY), new Point(-1, -1));

            // контуры упаковываются в иерархию
            var contours = new VectorOfVectorOfPoint();
            var hierarchy = new Mat();

            // прямоугольник, по которому мы срезаем участок с ответами
            var cropRectangle = new Rectangle(widthCrMin, heightCrMin, widthCrMax - widthCrMin, heightCrMax - heightCrMin);

            var frameTemp = new Image<Bgr, Byte>(frame.Bitmap);

            // выбираем ROI(область интересов) по которой будем срезать изображение и кропаем его
            var cropped = frame.Copy(cropRectangle);

            // предобработка изображения для контурного анализа
            CvInvoke.CvtColor(cropped, cropped, ColorConversion.Bgr2Gray);
            CvInvoke.Erode(cropped, cropped, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

            // если ответы слишком длинные и доходят до края изображения, не получится выделить замкнутый контур, для этого рисуем по краям прямоугольник
            CvInvoke.Rectangle(cropped, new Rectangle(0, 0, cropped.Width, cropped.Height), new MCvScalar(255, 255, 255),
                10, LineType.AntiAlias);

            // выделяем контуры
            CvInvoke.Canny(cropped, cropped, 200, 255, 3, true);

            CvInvoke.FindContours(cropped, contours, hierarchy, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            // проходиимся по контурам
            for (var i = 0; i < contours.Size; i++)
            {
                // получаем ROI, для чего получаем опоясывающий прямоугольник вокруг текста, далее по его координатам можем срезать нужный текст
                var brect = CvInvoke.BoundingRectangle(contours[i]);
                if (brect.Height > 10 && brect.Height < 30)
                    CvInvoke.Rectangle(frameTemp, new Rectangle(brect.X + widthCrMin, brect.Y + heightCrMin, brect.Width, brect.Height),
                        new MCvScalar(255, 0, 0), 1, LineType.AntiAlias);
            }
            return frameTemp;
        }

        public Image<Bgr, Byte> ResizeFrame(Image<Bgr, Byte> frame, int height, int width)
        {
            return frame.Resize(width, height, Inter.Cubic);
        }

        public Image<Bgr, Byte> RotateFrame(Image<Bgr, Byte> frame, int thresholdX)
        {
            double angle = 0;

            // ядро для морфологических операций
            var kernel = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(thresholdX, 1), new Point(-1, -1));

            // контуры упаковываются в иерархию
            var contours = new VectorOfVectorOfPoint();
            var hierarchy = new Mat();

            var frameTemp = new Image<Bgr, byte>(frame.Bitmap);

            // подготавливаем изображение к контурному анализу
            CvInvoke.CvtColor(frameTemp, frameTemp, ColorConversion.Bgr2Gray);
            CvInvoke.Erode(frameTemp, frameTemp, kernel, new Point(-1, -1), 1, BorderType.Constant, new MCvScalar(0));
            CvInvoke.Rectangle(frameTemp, new Rectangle(0, 0, frame.Width, frame.Height), new MCvScalar(255, 255, 255), 50, LineType.AntiAlias);
            CvInvoke.Canny(frameTemp, frameTemp, 100, 255, 3, false);
            CvInvoke.FindContours(frameTemp, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxSimple);
            for (var i = 0; i < contours.Size; i++)
            {
                var brect = CvInvoke.BoundingRectangle(contours[i]);
                var rrect = CvInvoke.MinAreaRect(contours[i]);
                // находим самый длинный контур на изображении
                if (brect.Width > 500 && brect.Height < 80 && brect.Y > 600)
                {
                    // выбиваем угол из опоясывающего прямоугольника
                    // помимо прочего чекаем направление поворота(по или против часовой стрелки повёрнуто изображение)
                    angle = rrect.Angle;
                    if (-angle > 45)
                        angle = 90 + angle;
                    break;
                }
            }

            // вращаем изображение на заданный угол
            frame = frame.Rotate(-angle, new Bgr(255, 255, 255));
            return frame;
        }
    }
}